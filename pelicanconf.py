#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'amapvichy'
SITENAME = 'AMAP Pays de Vichy'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Paris'
DEFAULT_DATE_FORMAT = '%d %B %Y'
DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Miramap', 'https://miramap.org/'),
         ('Réseau AMAP Auvergne Rhône-Alpes', 'https://amap-aura.org/'),)

# Social widget
"""
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)
"""

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# personnalisation
THEME = 'themes/html5-dopetrope'
STATIC_PATHS = ( "doc/", )

HEADER_TITLE = 'Tous les jeudis de 17h30 à 18h45'
HEADER_SUBTITLE = '2, place de la république à Cusset - à côté du Casino'
STAR_HEADER = 'AMAP'
STAR_TEXT = 'Les - Associations pour le Maintien d’une Agriculture Paysanne - sont destinées à favoriser l’agriculture paysanne et biologique qui a du mal à subsister face à l’agro-industrie.'
SOLIDAR_HEADER = 'Solidaire'
SOLIDAR_TXT = 'Le principe est de créer un lien direct entre paysans et consommateurs, qui s’engagent à acheter la production des premiers à un prix équitable et en payant par avance.'
ORGA_HEADER = 'Association'
ORGA_TEXT = 'Pour bénéficier d\'un panier, il est nécessaire d\'être membre de l\'association'
STATUTS_LINK = '../doc/statutsadoptes_ag2017.pdf'
STATUTS_LAB = 'nos statuts'
ABOUT_TEXT = 'AMAP Pays de Vichy est une association régie par la loi du 1er juillet 1901 et le décret du 16 août 1901.'
ABOUT_IMAGE = 'theme/images/logo.gif'
LIBRE_LINK = 'pages/producteurs.html'
LIBRE_LAB = 'Découvrir nos producteurs'
SOLIDAIRE_LINK = 'pages/distributions.html'
SOLIDAIRE_LAB = 'Comment bénéficier d\'un panier'
ABOUT_LINK = 'pages/qui-sommes-nous.html'
DISTRIBUTIONS = 'Tous les jeudis, de 17h30 à 18h45 au 2, place de la république à Cusset - à côté du Casino'
MAIL = 'amapvichy.org'
COPYRIGHT = 'AmapVichy. Sauf mention contraire, le contenu est disponible sous licence <a href="https://creativecommons.org/licenses/by-sa/3.0/fr/" target="_blank">CC-BY-SA version 3.0 ou ultérieure</a>'
CODE = '<i class="icon brands fa-gitlab" aria-hidden="true"></i> Hébergé sur <a href="https://framagit.org/moulinux/amapvichy.org" target="_blank">Framagit</a>'
PARTNERS = (('Pisciculture du Moulin Piat', 'http://www.pisciculturemoulinpiat.com/'),
            ('Terres de ROA', 'https://terresderoa.com/'),)
LOCALE = ('fr_FR.utf8', )
#MODAL_TEXT = """\
#Nous organisons le 17/10 de 15 h à 17 h, à l’Atrium de Vichy,
#un atelier d’installation du système libre GNU/Linux, afin de libérer vos
#machines de l’emprise des multinationales américaines.
#"""
#MODAL_LINK = 'install-party-17.html'
#MODAL_LAB = 'Inscription'
