Title: Distributions
Slug: distributions
Authors: amapvichy
Summary: Rendez-­vous hebdomadaires, les jeudis de 17h30 à 18h45 au 2, place de la république à Cusset - à côté du Casino.

![Plan distribution](../doc/osm_amap_mod.png "plan")

Vous pouvez nous rencontrer lors des distributions ou nous joindre électroniquement via notre mail de contact (bas de la page).
