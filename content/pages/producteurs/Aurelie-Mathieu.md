Title: Aurélie Mathieu
Slug: Aurelie-Mathieu
Authors: amapvichy
Summary: Maraîchage - légumes AB

Aurélie est installée au domaine de Chavagnat à Marcenat

![Aurélie](../../doc/aurelie-mathieu.jpg "Photo d'Aurélie")

Elle produit des légumes sous le label AB. Elle développe sa production en s'appuyant sur des variétés anciennes et sur la qualité gustative de ses légumes.


### Contrat

Vous pouvez télécharger les contrats au format [pdf] pour les imprimer et remplir en deux exemplaires.

> Un exemplaire devra être remis au producteur sur le lieu de livraison de l'Amap, accompagné de son règlement. Vous pouvez effectuer le paiement avec plusieurs  chèques. Dans ce cas, veuillez noter au dos de chaque chèque la date d'encaissement souhaitée.

[pdf]: ../doc/contrat_amap_aurelie_mathieu.pdf
[rtf]: ../doc/contrat_amap_aurelie_mathieu.rtf
